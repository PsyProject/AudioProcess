import numpy as np
from scipy.signal import butter, lfilter, freqz
import scipy.signal as sg
import matplotlib.pyplot as plt


"""
Class encapsulating the Butterworth lowpass filter of Scipy


"""

class ButterLowpassFilter(object):
    def __init__(self, norm_cutoff, fs, order=5):
        self._normalized_cutoff = norm_cutoff
        self._sampling_frequency = fs
        self._ready = False
        self.a = None
        self.b = None
        self._order = order
    
    def filter(self, signal):
        if not self._ready:
            print("[Info] Baking lowpass filter")
            self.bake_filter()
        return lfilter(self.b, self.a, signal)
    
    def bake_filter(self):
        nyq = 0.5 * self._sampling_frequency
        normal_cutoff = self._normalized_cutoff / nyq
        self.b, self.a = butter(self._order, normal_cutoff, btype='low', analog=False)
        self._ready = True

    def filterlr(self, signal):
        if not self._ready:
            print("[Info] Baking lowpass filter")
            self.bake_filter()
        zi = sg.lfilter_zi(self.b, self.a)
        z, _ = sg.lfilter(self.b, self.a, signal, zi=zi*signal[0])
        z2, _ = sg.lfilter(self.b, self.a, signal, zi=zi*z[0])
        return sg.filtfilt(self.b, self.a, signal)



def subSample(array, n):
    return array[::n]


def method1():
    pass



def smoothy2(sig, n=11025):
    y = []
    somme = 0
    nb_elem = 0

    # calculer la somme pour le premier élément du tableau
    for i in range(n + 1):
        if i < len(sig):
            nb_elem += 1
            somme += sig[i]

    y.append(somme / nb_elem)
    
    # DEBUG
    # print("pour un i de :", 0 )

    # pour chaque élément, 'décaler' la somme de 1 vers la droite
    for i in range(1, len(sig)):
        if i - n - 1 >= 0:
            somme -= sig[i - n - 1]
            nb_elem -= 1
        if i + n < len(sig):
            somme += sig[i + n]
            nb_elem += 1
        y.append(somme / nb_elem)

    # DEBUG
    # print("pour un i de :", i, " ", somme/nb_elem )
    return y



"""
Permet de synchroniser deux signaux .wav en entrée

"""

def synchro(sig1,sig2):
    longeur1 = len(sig1)
    posmax1 = np.argmax(sig1[:4000000])


    longeur2 = len(sig2)
    posmax2 = np.argmax(sig2[:4000000])

    # print("Synchro located clap at " + str(posmax1) + " for signal 1")
    # print("Synchro located clap at " + str(posmax2) + " for signal 2")

    #permet de synchroniser les deux signaux (à affiner: detection du maximum dans les 10 premières secondes sinon
    #des problemes peuvent se poser avec des coups dans le micro etc
    #il va faloir aussi retoucher toute les pistes pour qu'il n'y ai qu'un seul clap


    sig1 = sig1[posmax1:]
    sig2 = sig2[posmax2:]
    #normalement après cette opération, la synchro est éffectuée,
    #il ne reste plus qu'a avoir deux listes de meme longeur


    diff=abs(len(sig1) - len(sig2))
    upper = [0] * diff

    if longeur1 > longeur2:
        sig2synchro = np.concatenate((sig2, upper), axis=0)
        sig1synchro = sig1

        return sig1synchro, sig2synchro, posmax1, posmax2
    else:
        sig1synchro = np.concatenate((sig1, upper), axis=0)
        sig2synchro=sig2

        return sig1synchro, sig2synchro, posmax1, posmax2

def moyenne_groupe(data,taille=10000):
    """docstring"""

    d=[]
    for i in range(0,len(data),taille):
        somme = 0
        nb_elem = 0
        for j in range(taille):
            if i+j <len(data):
                somme = somme+data[i+j]
                nb_elem+=1
        moyenne= somme/nb_elem
        for j in range(taille):
            d.append(moyenne)
    return d

