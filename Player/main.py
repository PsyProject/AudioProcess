'''

Visualisation du résultat du code de Diarisation.

Unamur - Département de Psychologie


'''

# Using python internal backend
import tkinter as tk
import matplotlib
matplotlib.use("tkAgg")
from matplotlib.widgets import Button
import matplotlib.pyplot as plt

import pyaudio
import wave


import numpy as np
import time
#import sounddevice as sd

# Read and play audio (pygame temporary solution)
import soundfile as sf
#import pygame

# JSON for diarisation files
import json

from utils import synchro

class AudioFile(object):
    def __init__(self, path):
        self.signal, self.fs = sf.read(path)




med_audio_path = "test_tracks/med.wav"
pat_audio_path = "test_tracks/pat.wav"

freqs = np.arange(2, 20, 3)

# Loading audio file
track_med = AudioFile(med_audio_path)
track_pat = AudioFile(pat_audio_path)

if track_med.fs == track_pat.fs:
    fs = track_med.fs
else:
    print("[ERROR] Tracks sampling frequencies do not match: " + str(track_med.fs) + " (med) != " + str(track_pat.fs) + " (pat)")

y, fs = sf.read(med_audio_path)
y_pat, fs_pat = sf.read(med_audio_path)
y, y_pat, _, _ = synchro(y, y_pat)
sf.write("med.sync.wav", y*50, fs)
sf.write("pat.sync.wav", y_pat*50, fs)
t = np.linspace(0.0, y.size / fs, y.size)
# Loading diarization file
f = open("sample.diar.json", "r")
diarization = json.load(f)



# Init audio systems

# Pygame (old) version
"""
pygame.mixer.init()
pygame.mixer.music.load("pat.sync.wav")

"""

# PyAudio edition
pa = pyaudio.PyAudio()
wf = wave.open("pat.sync.wav")



# root = tk.Tk()
fig, ax = plt.subplots()

for section in diarization["diarization"]:
    if section["speaker"] == "doctor":
        ax.add_patch(matplotlib.patches.Rectangle((section["start-time"], -0.03),
            section["end-time"]-section["start-time"], 0.06, color="lightblue"))
    elif section['speaker'] == "patient":
        ax.add_patch(matplotlib.patches.Rectangle((section["start-time"], -0.03),
            section["end-time"]-section["start-time"], 0.06, color="orange")) 
    else:
        ax.add_patch(matplotlib.patches.Rectangle((section["start-time"], -0.03),
            section["end-time"]-section["start-time"], 0.06, color="black")) 


plt.subplots_adjust(bottom=0.2)
# Doctor is ABOVE
l, = plt.plot(t[::2000], y[::2000] + 0.013, lw=2)
# Patient is BELOW
l2, = plt.plot(t[::2000], y_pat[::2000] - 0.013, lw=2)
cursor, = plt.plot([0], [0], 'ro')
cursor2, = plt.plot([0, 0], [-0.05, 0.05], 'r-')


class Index(object):

    def __init__(self):
        self.ind = 0
        self.is_playing = None
        self.last_time = time.time()
        self.current_progress=0.0

        self.cursor_position = 0
    
    def callback_audio(self, in_data, frame_count, time_info, status):
        data = wf.readframes(frame_count)
        self.cursor_position += frame_count
        self.update()
        return (data, pyaudio.paContinue)


    def next(self, event):
        pass
    def prev(self, event):
        pass

    def play(self, event):
        #sd.play(100*y,fs)
        if self.is_playing is None:
            # pygame.mixer.music.play()
            stream.start_stream()
        if not self.is_playing:
            self.is_playing = True
            #pygame.mixer.music.unpause()
        else:
            #pygame.mixer.music.pause()
            stream.stop_stream()
            self.is_playing = False

    def stop(self, event):
        self.current_progress = 0.0
        self.is_playing = False
        #pygame.mixer.stop()

    def close(self, event):
        plt.close()
        exit()

    def update(self):
        self.is_playing = True
        if self.is_playing:
            # Compute additional playing time
            #
            # elapsed_time = time.time() - self.last_time
            # self.current_progress += elapsed_time
            self.current_progress = self.cursor_position / fs 
            # Update cursor display
            #
            cursor.set_xdata([self.current_progress])
            cursor2.set_xdata([self.current_progress, self.current_progress])
            ax.set_xlim([self.current_progress-5.0, self.current_progress+5.0])
            ax.set_ylim([-0.025, 0.025])
            plt.draw()
            #time.sleep(0.005)
        # Get roughly 60 fps
        # root.after(17, self.update)
        # self.last_time = time.time()

callback = Index()




# open stream using callback (3)
stream = pa.open(format=pa.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True,
                stream_callback=callback.callback_audio)

# stream.stop_stream()
axclose = plt.axes([0.37, 0.05, 0.1, 0.075])
axstop = plt.axes([0.48, 0.05, 0.1, 0.075])
axprev = plt.axes([0.59, 0.05, 0.1, 0.075])
axnext = plt.axes([0.7, 0.05, 0.1, 0.075])
axplay = plt.axes([0.81, 0.05, 0.1, 0.075])

bclose = Button(axclose, 'Close')
bclose.on_clicked(callback.close)
bstop = Button(axstop, 'Stop')
bstop.on_clicked(callback.stop)
bnext = Button(axnext, 'Forward')
bnext.on_clicked(callback.next)
bprev = Button(axprev, 'Prev')
bprev.on_clicked(callback.prev)
bplay = Button(axplay, 'Play')
bplay.on_clicked(callback.play)


plt.show(block=True)

exit()
while stream.is_active():
    callback.update()
    plt.draw()
    plt.show(block=False)


plt.show(block=True)
