#!/usr/bin/env python3.6
# coding: utf-8

def smoothy(sig):
    n=11025
    y=[]
    for i in range(len(sig)):
        nb_elem = 0
        somme = 0

        for j in range(i - n, i + n + 1):

            if j >= 0 and j < len(sig):
                nb_elem += 1
                somme += sig[j]
        moyloc= somme/nb_elem
        y.append(moyloc)
        print("pour un i de :", i, " ", moyloc )
    return y

def smoothy2(sig,n=11025):
    y=[]
    somme = 0
    nb_elem = 0
    
    #calculer la somme pour le premier élément du tableau
    for i in range(n+1):
        if i < len(sig):
            nb_elem += 1
            somme += sig[i]
    
    y.append(somme/nb_elem)
   # print("pour un i de :", 0 )
    
    #pour chaque élément, 'décaler' la somme de 1 vers la droite
    for i in range(1, len(sig)):
        if i-n-1 >= 0:
            somme -= sig[i-n-1]
            nb_elem -= 1
        if i+n < len(sig):
            somme += sig[i+n]
            nb_elem += 1
        y.append(somme/nb_elem)
       # print("pour un i de :", i, " ", somme/nb_elem )
    return y
