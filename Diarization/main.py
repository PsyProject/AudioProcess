#!/bin/python3
# coding: utf-8


'''
@Title
Dual track interview diarization.

@Description


-----------------------------------------------
@Authors:
    -> Jennequin Kevin
    -> Roy Nicolas
'''



'''
@imports

'''

import os
import sys
import json
import argparse

# Use python's own gui backend
import matplotlib
matplotlib.use('tkAgg')
from matplotlib import pyplot as plt

import soundfile as sf
import numpy as np

from utils import ButterLowpassFilter, subSample, smoothy2, synchro

from skfuzzy import control as ctrl

def diarize(track1_path, track2_path, method='crisp'):

    # Ouverture des deux pistes audios avec le module soundfile
    sig1, fs1 = sf.read(track1_path)
    sig2, fs2 = sf.read(track2_path)
    
    # Nous véfions la cohérence des fréquences d'échantillonage
    if fs1 != fs2:
        print('[ERROR] Sampling frequencies not matching.')
        exit()
    fs = fs1
    
    # Synchronisation des deux pistes audio
    sig1_sync, sig2_sync, clap1, clap2 = synchro(sig1, sig2)
    
    # Nous exportons les listes en np.ndarray
    sig1_sync = np.array(sig1_sync)
    sig2_sync = np.array(sig2_sync)
    
    # Renormalisation médianne des signaux
    sig1_median = np.mean(np.abs(sig1_sync))
    sig2_median = np.mean(np.abs(sig2_sync))

    sig1_sync = sig1_sync / sig1_median
    sig2_sync = sig2_sync / sig2_median
   
    '''
    
    Application du filtre passe-bas

    '''
    # Paramèes du filtre
    order = 3           # Ordre du filtre
    fs = 44100          # Fréquence d'éantillonage, Hz
    cutoff = 22100      # Fréquence de coupure, Hz
    
    # Préparation pour comparaison
    sig1_comp =(sig1_sync) ** 2
    sig2_comp =(sig2_sync) ** 2
    

    lowpassFilter = ButterLowpassFilter(0.1, fs, order=order)
    sig1_low = lowpassFilter.filterlr(sig1_comp)
    sig2_low = lowpassFilter.filterlr(sig2_comp)
    
    #sig1_low = np.array(smoothy2(sig1_comp, 2))
    #sig2_low = np.array(smoothy2(sig2_comp, 2))
    
    signals_mean = (np.mean(sig1_low) + np.mean(sig2_low)) / 2.0
    
    #
    # Displaying results
    #
    
    speaking = sig1_low < sig2_low
    speaking_ratio = sig1_low / sig2_low
    
    
    time = np.array(range(len(sig1_comp)))/fs
    time_disp, sig1_low_disp, sig2_low_disp, sig1_comp_disp, sig2_comp_disp, speaking_disp = \
    map(subSample, (time, sig1_low, sig2_low, sig1_comp, sig2_comp, speaking), [1000]*6)

    # fig, ax = plt.subplots(2,1)
    # plt.axis([0, len(sig1)/fs, 0, 100])
    # ax[0].plot(time_disp, sig1_comp_disp, label="Raw Signal")
#    ax[0].plot(time_disp, sig1_low_disp, label="Signal lowpass med")
#    ax[0].plot(time_disp, sig2_low_disp, label="Signal lowpass pat")
#    ax[1].set_ylim(0, 1) 
#    plt.legend()
#    ax[1].plot(time_disp, speaking_disp, label="Signal lowpass pat")
    
     
    # plt.title("Effet du filtre pass-bas")
    # plt.legend()
    # plt.show(block=True)

    if method == 'crisp':
        print("INFO: Building digest file.")
        
        diarization = list()
        current_session = None
        patient_speaking = None
        for i in range(len(speaking)):
            if patient_speaking is None:
                current_session = dict()
                patient_speaking = speaking[i]
                current_session["start-time"] = float(i / fs)
            
            
            if patient_speaking == speaking[i]:
                pass
                # Patient still speaking or doctor still speaking
            elif patient_speaking != speaking[i]:
                # Changed !
                if patient_speaking:
                    current_session["speaker"] = "patient"
                else:
                    current_session["speaker"] = "doctor"
                current_session["end-time"] = float(i / fs)
                diarization.append(current_session)
                current_session = dict()
                current_session["start-time"] = float(i / fs)
                patient_speaking = speaking[i]
    
    else:
        diarization = list()
        current_session = None
        current_speaker = None

        for i in range(len(speaking_ratio)):
            print("processing")
            ratio = speaking_ratio[i]
            signal = (sig1_low[i] + sig2_low[i]) / 2.0
            
            # First, who is speaking ?
            #
            if abs(ratio - 1.0 ) < 0.25:
                # Both are kinda equal
                if signal < signals_mean * 2.0:
                    # Silence
                    current_speaker = 'silence'
                else:
                    current_speaker = 'both'

            elif ratio > 1.25:
                current_speaker = 'patient'
            
            elif ratio < 0.75:
                current_speaker = 'doctor'
  
  # Now, we create or update the current session.
            if current_session is None:
                current_session = dict()
                current_session['speaker'] = current_speaker
                current_session['start-time'] = float(i / fs)
            elif current_session['speaker'] == current_speaker:
                pass
            elif current_session['speaker'] != current_speaker:
                current_session['end-time'] = float(i / fs)
                # Saving the finished session
                diarization.append(current_session)
                current_session = dict()
                current_session['speaker'] = current_speaker
                current_session['start-time'] = float(i / fs)

    # Saving whatever data we generated
    json_data = json.loads("{}")   
    json_data["diarization"] = diarization
    
    f = open("sample.diar.json", "w")
    
    f.write(json.dumps(json_data, indent=4, sort_keys=True))
    f.close()
    print("done !") 
    
    plt.plot(time[10::1000], -15000 * speaking[10::1000], label="Signal lowpass pat")
  

if __name__ == '__main__':
    # Argument management 
    parser = argparse.ArgumentParser(description='Synchronize and diarize a stereo recording.')
    parser.add_argument('audiofiles', metavar='Track', type=str, nargs=2, help='Audio tracks for the two microphones')
    parser.add_argument('--method', dest="method", help='Method for speaker dicretisation: <fuzzy> or <crisp>')
    args = parser.parse_args()
    diarize(args.audiofiles[0], args.audiofiles[1], method=args.method)





