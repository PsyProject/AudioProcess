#!/usr/bin/env python3.6
# coding: utf-8

import numpy as np
from smoothy import smoothy
sig=np.linspace(0,1000,1000)
sig=np.array(sig)
y=smoothy(sig)

print(y)